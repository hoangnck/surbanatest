import { ApiProperty } from '@nestjs/swagger'

export class GetAllDto {
  @ApiProperty({
    description: `Page size`,
    name: 'pageSize',
    required: false,
  })
  pageSize?: number

  @ApiProperty({
    description: `Page number`,
    name: 'pageNumber',
    required: false,
  })
  pageNumber?: number

  @ApiProperty({
    description: `Sort`,
    name: 'order',
    required: false,
  })
  order?: string

  @ApiProperty({
    description: `Filter`,
    name: 'where',
    required: false,
  })
  where?: string

  @ApiProperty({
    description: `Select`,
    name: 'select',
    required: false,
  })
  select?: string
}
