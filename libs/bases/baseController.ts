import { User } from '#/decorators/user.decorator'
import { TokenUser } from '@/types/common'
import { Body, Delete, Get, Param, Post, Put, Query } from '@nestjs/common'
import { ApiOperation } from '@nestjs/swagger'

import { GetAllDto } from './baseDto'
import { BaseEntity } from './baseEntity'
import { BaseService } from './baseService'

export class BaseController<T extends BaseEntity, F extends BaseService<T>> {
  private readonly baseService: F
  constructor(baseService: F) {
    this.baseService = baseService
  }

  @Get()
  @ApiOperation({ summary: 'Get items' })
  async get(@Query() getAllDto: GetAllDto) {
    return await this.baseService.get(getAllDto)
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get detail item by id' })
  async getDetail(@Param('id') id: string) {
    return await this.baseService.getDetail(id)
  }

  @Post()
  @ApiOperation({ summary: 'Create item' })
  async post(@User() user: TokenUser, @Body() itemDto: Partial<T>) {
    return await this.baseService.post(itemDto, user)
  }

  @Put(':id')
  @ApiOperation({ summary: 'Edit item' })
  async put(@User() user: TokenUser, @Param('id') id: string, @Body() itemDto: Partial<T>) {
    return await this.baseService.put(user, id, itemDto)
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete item' })
  async delete(@User() user: TokenUser, @Param('id') id: string) {
    return await this.baseService.delete(user, id)
  }

  @Delete('destroy/:id')
  @ApiOperation({ summary: 'Destroy item' })
  async destroy(@Param('id') id: string) {
    return await this.baseService.destroy(id)
  }
}
