import { ApiResponse, Meta, TokenUser } from '@/types/common'
import { Injectable } from '@nestjs/common'
import { DeleteResult, FindOptionsWhere, ILike, In, Not, Repository } from 'typeorm'

import { GetAllDto } from './baseDto'
import { BaseEntity } from './baseEntity'

@Injectable()
export class BaseService<T extends BaseEntity> {
  public readonly baseRepository: Repository<T>
  private readonly newEntity: (data: Partial<T>) => T

  constructor(baseRepository: Repository<T>, newEntity: (data: Partial<T>) => T) {
    this.baseRepository = baseRepository
    this.newEntity = newEntity
  }

  async setMeta<TData = unknown>(
    data: Promise<TData> | TData,
    message: string,
    meta?: Meta,
  ): Promise<ApiResponse<TData>> {
    const rs = await data
    return {
      data: rs,
      meta: { message, ...meta },
    }
  }

  async get(getAllDto: GetAllDto) {
    const { pageSize, pageNumber, order } = getAllDto
    const where = (getAllDto.where && JSON.parse(getAllDto.where)) || {}
    const select = (getAllDto.select && JSON.parse(getAllDto.select)) || null
    if (where instanceof Array) {
      for (const item of where) {
        Object.keys(item).forEach(key => {
          if (item[key].operation === 'like') {
            item[key] = ILike(item[key].value)
          } else if (item[key].operation === 'in') {
            item[key] = In(item[key].value)
          } else if (item[key].operation === 'notIn') {
            item[key] = Not(In(item[key].value))
          }
        })
      }
    } else {
      Object.keys(where).forEach(key => {
        if (where[key].operation === 'like') {
          where[key] = ILike(where[key].value)
        } else if (where[key].operation === 'in') {
          where[key] = In(where[key].value)
        } else if (where[key].operation === 'notIn') {
          where[key] = Not(In(where[key].value))
        }
      })
    }
    const [items, total] = await this.baseRepository.findAndCount({
      where,
      select,
      order: { ...JSON.parse(order || '{}') },
      skip: pageNumber,
      take: pageSize,
    })

    return this.setMeta<T[]>(items, 'Get items successfully!', { total })
  }

  getDetail(id: string) {
    const where = { id } as FindOptionsWhere<T>
    return this.setMeta<T>(
      this.baseRepository.findOne({
        where,
      }),
      'Get detail item successfully!',
    )
  }

  post(itemDto: Partial<T>, user?: TokenUser) {
    return this.setMeta<T>(
      this.baseRepository.save(
        this.newEntity({ ...itemDto, createdBy: user.id, createdAt: new Date() }),
      ),
      'Post item successfully!',
    )
  }

  put(user: TokenUser, id: string, itemDto: Partial<T>) {
    return this.setMeta<T>(
      this.baseRepository.save(
        this.newEntity({ ...itemDto, id, updatedAt: new Date(), updatedBy: user.id }),
      ),
      'Edit item successfully!',
    )
  }

  delete(user: TokenUser, id: string) {
    return this.setMeta<any>(
      this.baseRepository.save(
        this.newEntity({ id, deletedAt: new Date(), deletedBy: user.id } as T),
      ),
      'Delete item successfully!',
    )
  }

  async destroy(id: string) {
    return this.setMeta<DeleteResult>(this.baseRepository.delete(id), 'Destroy item successfully!')
  }
}
