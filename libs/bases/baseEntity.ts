import { ApiProperty } from '@nestjs/swagger'
import { Exclude } from 'class-transformer'
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'

export abstract class BaseEntity {
  constructor(baseEntity: Partial<BaseEntity>) {
    const keys = [
      'id',
      'createdBy',
      'createdAt',
      'updatedBy',
      'updatedAt',
      'deletedBy',
      'deletedAt',
    ]
    baseEntity &&
      keys.forEach(key => {
        baseEntity[key] !== undefined && (this[key] = baseEntity[key])
      })
  }

  @ApiProperty({ description: `Id` })
  @PrimaryGeneratedColumn('uuid')
  id: string

  @ApiProperty({ description: `Created by` })
  @Column({ type: 'uuid', nullable: true })
  createdBy: string

  @ApiProperty({ description: `Created at` })
  @CreateDateColumn()
  createdAt: Date

  @ApiProperty({ description: `Updated by` })
  @Column({ type: 'uuid', nullable: true })
  updatedBy: string

  @ApiProperty({ description: `Updated at` })
  @UpdateDateColumn()
  updatedAt: Date

  @Exclude()
  @ApiProperty({ description: `Deleted by` })
  @Column({ type: 'uuid', nullable: true })
  deletedBy: string

  @Exclude()
  @ApiProperty({ description: `Deleted at` })
  @DeleteDateColumn()
  deletedAt: Date
}
