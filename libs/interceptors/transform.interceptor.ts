import { DataHttpException } from '#/data.exception'
import { ErrorCode } from '@/contansts'
import { ApiResponse } from '@/types/common'
import {
  CallHandler,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NestInterceptor,
  RequestTimeoutException,
} from '@nestjs/common'
import { Observable, TimeoutError, throwError } from 'rxjs'
import { catchError, map, timeout } from 'rxjs/operators'

@Injectable()
export class TransformInterceptor<T> implements NestInterceptor<T, ApiResponse<T>> {
  async intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Promise<Observable<ApiResponse<T>>> {
    return next.handle().pipe(
      map((item: ApiResponse<T>) => {
        if (Object.keys(item || {}).some(key => !['data', 'meta'].includes(key))) {
          throw new DataHttpException({
            code: ErrorCode.WrongDataStructure,
            message: 'Wrong data structure',
            statusCode: HttpStatus.EXPECTATION_FAILED,
          })
        }
        return {
          data: item.data,
          meta: {
            success: true,
            code: 'Oke',
            message: 'Successfully!',
            ...(item.meta || {}),
          },
        }
      }),
      timeout(10000),
      catchError(err => {
        if (err instanceof TimeoutError) {
          return throwError(() => new RequestTimeoutException(err.message))
        }

        if (err instanceof HttpException) {
          return throwError(() => err)
        }

        return throwError(() => new InternalServerErrorException(err.message))
      }),
    )
  }
}
