import { Role } from '@/contansts'
import { SetMetadata } from '@nestjs/common'

export const Roles = (...roles: Role[]) => SetMetadata('roles', roles)
export const SuperAdminRole = () => SetMetadata('roles', [Role.SuperAdmin])
export const AdminRole = () => SetMetadata('roles', [Role.Admin])
export const SecretaryRole = () => SetMetadata('roles', [Role.Secretary])
export const UserRole = () => SetMetadata('roles', [Role.User])
