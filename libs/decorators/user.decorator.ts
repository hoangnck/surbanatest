import { TokenUser } from '@/types/common'
import { ExecutionContext, createParamDecorator } from '@nestjs/common'
import { get } from 'lodash'

export const User = createParamDecorator((fieldName: string, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest()
  const user = request.user as TokenUser

  return fieldName ? get(user, 'fieldName') : user || {}
})
