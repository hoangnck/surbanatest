import { BaseFunc } from '@/contansts'
import { CanActivate, ExecutionContext, ForbiddenException, Injectable } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { get } from 'lodash'

@Injectable()
export class RolesGuard implements CanActivate {
  private reflector: Reflector

  constructor(
    private readonly roles?: {
      all?: string[]
      read?: string[]
      edit?: string[]
      // func
      get?: string[]
      findOne?: string[]
      detail?: string[]
      post?: string[]
      put?: string[]
      delete?: string[]
      destroy?: string[]
      deletes?: string[]
      detroys?: string[]
    },
  ) {
    this.reflector = new Reflector()
  }

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest()

    const user = request.user
    if (request.bypassAuth) {
      return true
    }

    if ((this.roles.all || []).includes(user.type)) {
      return true
    }

    const funcName = this.reflector.get<string>('funcName', context.getHandler()) as BaseFunc
    const roles =
      this.reflector.get<string[]>('roles', context.getHandler()) ||
      get(this.roles, funcName) ||
      ([BaseFunc.Detail, BaseFunc.FindOne, BaseFunc.Get].includes(funcName)
        ? this.roles.read
        : this.roles.edit)

    if (roles) {
      if (roles.length === 0 || roles.includes(user.type)) {
        return true
      }
    } else {
      if (!this.roles.all) return true
    }

    throw new ForbiddenException('Incorrect role!')
  }
}
