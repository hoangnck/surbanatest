import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common'
import { Reflector } from '@nestjs/core'

@Injectable()
export class BaseGuard implements CanActivate {
  private reflector: Reflector

  constructor(private readonly passItems: string[]) {
    this.reflector = new Reflector()
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const funcName = this.reflector.get<string>('funcName', context.getHandler())
    const request = context.switchToHttp().getRequest()
    request.bypassAuth = this.passItems.includes(funcName)

    return true
  }
}
