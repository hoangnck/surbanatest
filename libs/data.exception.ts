import { HttpException, HttpStatus } from '@nestjs/common'

export class DataHttpException extends HttpException {
  constructor(config: { code?: string; statusCode?: number; message: string; error?: any }) {
    super(
      {
        statusCode: HttpStatus.NOT_IMPLEMENTED,
        ...config,
      },
      config.statusCode || HttpStatus.NOT_IMPLEMENTED,
    )
  }
}
