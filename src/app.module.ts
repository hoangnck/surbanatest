import { TransformInterceptor } from '#/interceptors/transform.interceptor'
import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { APP_INTERCEPTOR } from '@nestjs/core'
import { TypeOrmModule } from '@nestjs/typeorm'

import { Building } from './modules/building/building.entity'
import { BuildingModule } from './modules/building/building.module'

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return {
          type: 'postgres',
          host: configService.get('host'),
          port: +configService.get('db_port'),
          username: configService.get('username'),
          password: configService.get('password'),
          database: configService.get('database'),
          logger: 'advanced-console',
          logging: false,
          synchronize: true,
          entities: [Building], //__dirname + '/**/*.entity{.ts,.js}'
        }
      },
      inject: [ConfigService],
    }),
    BuildingModule,
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
  ],
})
export class AppModule {}
