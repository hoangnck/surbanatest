export interface Meta {
  code?: string
  message?: string
  name?: string
  success?: boolean
  status?: boolean
  error?: any
  total?: number
}
export interface ApiResponse<TData> {
  meta?: Meta
  data?: TData
}

export type TokenUser = {
  username: string
  email: string
  id: string
  roles: string[]
}
