import { BaseController } from '#/bases/baseController'
import { Controller, Get, Param } from '@nestjs/common'
import { ApiOperation, ApiTags } from '@nestjs/swagger'

import { Building } from './building.entity'
import { BuildingService } from './building.service'

@ApiTags('Building api')
@Controller('buildings')
export class BuildingController extends BaseController<Building, BuildingService> {
  constructor(private readonly buildingService: BuildingService) {
    super(buildingService)
  }

  @Get('tree/:id')
  @ApiOperation({ summary: 'Get a location tree' })
  async getTree(@Param('id') id: string) {
    return await this.buildingService.getTree(id)
  }
}
