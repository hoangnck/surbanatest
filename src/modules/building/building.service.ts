import { BaseService } from '#/bases/baseService'
import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

import { Building } from './building.entity'
import { getLocationTree } from './queries'

@Injectable()
export class BuildingService extends BaseService<Building> {
  constructor(
    @InjectRepository(Building)
    private readonly buildingRepository: Repository<Building>,
  ) {
    super(buildingRepository, data => new Building(data))
  }

  getTree(id: string) {
    return this.setMeta<Building[]>(
      this.baseRepository.query(getLocationTree(id)),
      'Get location tree successfully!',
    )
  }
}
