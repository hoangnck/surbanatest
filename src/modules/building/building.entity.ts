import { BaseEntity } from '#/bases/baseEntity'
import { ApiProperty } from '@nestjs/swagger'
import { Column, Entity } from 'typeorm'

@Entity('building')
export class Building extends BaseEntity {
  constructor(
    building?: Partial<Building>,
    keys: string[] = ['name', 'area', 'locationNumber', 'parentId'],
  ) {
    super(building)
    building &&
      keys.forEach(key => {
        building[key] !== undefined && (this[key] = building[key])
      })
  }

  @ApiProperty({ description: `Name location` })
  @Column({ nullable: true })
  name: string

  @ApiProperty({ description: `Area` })
  @Column({ nullable: true })
  area: number

  @ApiProperty({ description: `Location number` })
  @Column({ nullable: true })
  locationNumber: string

  @ApiProperty({ description: `Parent id` })
  @Column({ type: 'uuid', nullable: true })
  parentId: string
}
