export const getLocationTree = (id: string) => `
  WITH RECURSIVE childs AS
  (
      SELECT 
          t1.id, 
          t1."parentId", 
          t1.name
      FROM 
          public.building t1
      WHERE 
          t1.id = get_parent('${id}')
      UNION ALL
      SELECT 
          t2.id, 
          t2."parentId", 
          t2.name
      FROM 
          public.building t2 
      INNER JOIN 
          childs ON childs.id = t2."parentId"
  )
  SELECT
      id,
      "parentId",
      name
  FROM
      childs;
`
