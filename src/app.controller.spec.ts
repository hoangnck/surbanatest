import { Test, TestingModule } from '@nestjs/testing'
import * as mocks from 'node-mocks-http'

import { AppController } from './app.controller'
import { AppService } from './app.service'

describe('AppController', () => {
  let appController: AppController

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile()

    appController = app.get<AppController>(AppController)
  })

  describe('root', () => {
    it('should return "Hello World!"', () => {
      const req = mocks.createRequest({
        method: 'GET',
        url: '/',
        params: {
          name: 'name',
        },
      })
      expect(appController.getHello(req).data).toBe('Hello World!')
    })
  })
})
