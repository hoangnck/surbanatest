import { Controller, Get, Req } from '@nestjs/common'
import { Request } from 'express'

import { AppService } from './app.service'

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(@Req() req: Request) {
    console.log('cookie: nck:', req.cookies.nck)
    return this.appService.getHello()
  }
}
