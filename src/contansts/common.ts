export enum ErrorCode {
  WrongDataStructure = 'WrongDataStructure',
}

export enum BaseFunc {
  Get = 'get',
  Detail = 'detail',
  FindOne = 'find-one',
  Post = 'post',
  Put = 'put',
  Delete = 'delete',
  Destroy = 'destroy',
  Deletes = 'deletes',
  Destroys = 'destroys',
}

export enum Role {
  User = 'user',
  Admin = 'admin',
  SuperAdmin = 'superadmin',
  Secretary = 'secretary',
}

export enum PostType {
  Draft = 'draft',
  All = 'all',
  Friend = 'friend',
  Private = 'private',
}

export enum Gender {
  Male = 'male',
  Famale = 'famale',
}
