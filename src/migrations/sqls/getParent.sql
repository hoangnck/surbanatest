CREATE FUNCTION get_parent(node_id uuid)
RETURNS uuid AS
$$
  WITH RECURSIVE get_parent AS
  (
      SELECT 
          t1.id, 
          t1."parentId", 
          t1.name, 
          0 AS level
      FROM 
          public.building t1
      WHERE 
          t1.id = node_id
      UNION ALL
      SELECT 
          t2.id, 
          t2."parentId", 
          t2.name,
          level+1
      FROM 
          public.building t2 
      INNER JOIN 
          get_parent ON get_parent."parentId" = t2.id
  )
  SELECT
      id
  FROM
      get_parent
  ORDER BY
      level DESC
  LIMIT 1 ;
$$
LANGUAGE SQL;