import * as fs from 'fs'
import { join } from 'path'
import { MigrationInterface, QueryRunner } from 'typeorm'

export class GetParent1712135546664 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log(232323, __dirname)
    const queries = fs
      .readFileSync(join(__dirname, '../../../migrations/sqls/getParent.sql'))
      .toString()

    await queryRunner.query(queries)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP FUNCTION IF EXISTS get_parent;`)
  }
}
