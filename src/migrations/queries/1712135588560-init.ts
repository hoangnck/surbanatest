import * as fs from 'fs'
import { join } from 'path'
import { MigrationInterface, QueryRunner } from 'typeorm'

export class Init1712135588560 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log(232323, __dirname)
    const queries = fs
      .readFileSync(join(__dirname, '../../../migrations/sqls/initData.sql'))
      .toString()

    await queryRunner.query(queries)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    return
  }
}
