import { DataSource } from 'typeorm'

import * as config from '../../migration.json'

export const dataSource = new DataSource({
  type: 'postgres',
  entities: [`${__dirname}/../**/*.entity.{ts,js}`],
  logging: true,
  synchronize: false,
  migrationsRun: false,
  migrations: [`${__dirname}/../migrations/queries/**/*.{ts,js}`],
  migrationsTableName: 'migrations_history',
  ...config,
})
