# SurbanaTest

## Edit .env and migration.json file

You have to change values about database connection for project and migration

## Start project

- yarn start:dev (start project)
- yarn mig:run (database migration)

## Swagger

http://localhost:3000/swagger

## Affter migration and start, you can call api get:

http://localhost:3000/buildings/tree/d33cdca6-244b-4751-b688-b99aa57e339b
